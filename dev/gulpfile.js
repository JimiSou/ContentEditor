var gulp = require('gulp'),
	concat = require('gulp-concat'),
  minify = require('gulp-minify');

//----------- JS -----------//
gulp.task('js', function() {
  gulp.src(['dev/scripts/jquery-3.3.1.js','dev/scripts/bootstrap.js','dev/scripts/contentEditorScript.js','dev/scripts/popper.js','dev/scripts/jquery-ui.js'])
    .pipe(concat('global.js'))
    .pipe(minify({
      ext:{
        min:'.min.js'
      }
  }))
    .pipe(gulp.dest('prod'));
});

gulp.task('compress', function() {
  gulp.src('./app/public/js/*')
  .pipe(minify({
    ext:{
      min:'.min.js'
    },
    exclude: ['tasks'],
    ignoreFiles: ['.combo.js', '-min.js']
  }))
  .pipe(gulp.dest('./app/public/js'))
});

gulp.task('default',['js'], function() {
  console.log('DONE!');
});
