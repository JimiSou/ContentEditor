<?php include ('safe.php');?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>CE</title>
  <link rel="stylesheet" type="text/css" href="css/loader.css">
  <script src="scripts/loader.js"></script>
  <link rel="stylesheet" type="text/css" href="css/Template_Lumen/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/contentEditorStyles.css">
  <link rel="stylesheet" type="text/css" href="css/icofont.css">
  <link href="css/quill.snow.css" rel="stylesheet">
</head>

<body>
  <div id="wrapper">
    <nav></nav>
    <section>
      <form id="form">
        <h1 id="editorTitle">Upravit stránku</h1>
        <div class="form-group">
          <label class="col-form-label">#</label>
          <input class="form-control" type="text" id="formId" readonly>
          <label class="col-form-label">Datum a čas vytvoření</label>
          <input class="form-control" type="date" id="formDateCreated" readonly><input class="form-control" type="time" id="formTimeCreated" readonly>
        </div>
        <div class="form-group">
          <label class="col-form-label col-form-label-lg">Zobrazit v menu</label>
          <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="formInMenu" checked="">
            <label class="custom-control-label" for="formInMenu">Zobrazit v menu</label>
          </div>

        </div>
        <div class="form-group">
          <label class="col-form-label col-form-label-lg">Titulek stránky</label>
          <input class="form-control" type="text" placeholder="Text titulku..." id="formTitle" data-validation="required" data-validation-error-msg="Toto pole je povinné" value="Titulek">
        </div>
        <div class="form-group">
          <label class="col-form-label col-form-label-lg">Popis stránky</label>
          <textarea class="form-control" type="text" placeholder="Text popisu..." id="formDescription" data-validation="required" data-validation-error-msg="Toto pole je povinné">Popis</textarea>
        </div>
        <div class="form-group">
          <label class="col-form-label col-form-label-lg" for="inputLarge">Obsah stránky</label>
          <div id="editor"></div>
        </div>
        <div class="form-group" >
          <label class="col-form-label col-form-label-lg" for="inputLarge">Viditelnost stránky</label><br>
          <div class="form-group">
            <div class="custom-control custom-radio">
              <input type="radio" id="customRadio1" name="publicOptions" value="1" class="custom-control-input" checked="">
              <label class="custom-control-label" for="customRadio1">Veřejná</label>
            </div>
            <div class="custom-control custom-radio">
              <input type="radio" id="customRadio2" name="publicOptions" value="date" class="custom-control-input">
              <label class="custom-control-label" for="customRadio2">Odložit k datu a času</label>
            </div>
            <div class="custom-control custom-radio">
              <input type="radio" id="customRadio3" name="publicOptions" value="0" class="custom-control-input">
              <label class="custom-control-label" for="customRadio3">Neveřejná</label>
            </div>
          </div>
        </div>
        <div class="form-group" id="publicDate" style="display:none;">
          <label class="col-form-label col-form-label-lg">Datum a čas zveřejnění</label>
          <input class="form-control" type="date" id="formDatePublic"><input class="form-control" type="time" id="formTimePublic">
        </div>
        <button type="submit" class="btn btn-success btn-lg">Uložit</button>
      </form>
    </section>
    <div class="alert alert-dismissible alert-secondary">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <span id="messageBox"></span>
</div>
  </div>
  <div id="loader"><img src="images/loader.svg"></div>
  <script src="dev/prod/global.min.js"></script>
  <script src="scripts/quill.min.js"></script>
  <script src="scripts/jqueryformvalidator.min.js"></script>
  <script>
    $(function() {


      $('.breadcrumb').append('<li class="breadcrumb-item"><a href="pages.php">Stránky</a></li><li class="breadcrumb-item active">Upravit Stránku</li>');
      $.validate({});
      //QUILL INIT
      var toolbarOptions = [
        [{
          'font': []
        }],
        ['bold', 'italic', 'underline', 'strike'], // toggled buttons
        [{
          'color': []
        }, {
          'background': []
        }], // dropdown with defaults from theme
        // ['code-block'],
        [{
          'header': 1
        }, {
          'header': 2
        }], // custom button values
        [{
          'size': ['small', false, 'large', 'huge']
        }], // custom dropdown
        [{
          'align': []
        }],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }],
        // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        ['link', 'image'],
        ['clean'] // remove formatting button
      ];
      // Handlers can also be added post initialization
      var quill = new Quill('#editor', {
        modules: {
          toolbar: toolbarOptions
        },
        theme: 'snow'
      });
      var urlParams = new URLSearchParams(window.location.search)
      var id=urlParams.get('id');
      if(id==null)
      {
        $('#formId').val('_' + Math.random().toString(36).substr(2, 9));
        $("#formDateCreated").attr("value", dateNow());
        $("#formTimeCreated").attr("value", timeNow());
        $('.breadcrumb-item.active').text('Nová stránka');
        $('#editorTitle').text('Nová stránka');
      }
      else {
        $.getJSON("data/pages.json", function(data) {
            $.each( data, function( key, val ) {
              if(id==val.id)
              {
                $('#formId').val(val.id);
                var date = val.created.split(" ");
                $("#formDateCreated").attr("value", date[0]);
                $("#formTimeCreated").attr("value", date[1]);
                if(val.inMenu=='false')
                {
                  $('#formInMenu').click();
                }
                $('#formTitle').val(val.title);
                $('#formDescription').val(val.description);
                quill.root.innerHTML=val.content;
                $('#editor').innerHTML = val.content;
                if(val.public==1)
                {
                  $('#customRadio1').click();
                }
                if(val.public=='date') {
                  $('#customRadio2').click();
                }
                if(val.public==0) {
                  $('#customRadio3').click();
                }
                $('#messageBox').html('Data '+id+' načtena!!').trigger('update');

              }
              console.log(val);
            });
          });
      }



      //FORM SUBMIT
      $('form').on('submit', function(e) {
        e.preventDefault();
        var formData = {'data':{'id':'','created':'','inMenu':'','title':'','description':'','content':'','public':'','publicDate':'','type':''}};
        formData['data']['id'] = $('#formId').val();
        formData['data']['created'] = $('#formDateCreated').val() + ' ' + $('#formTimeCreated').val()
        formData['data']['inMenu'] = $('#formInMenu').prop('checked');
        formData['data']['title'] = $('#formTitle').val();
        formData['data']['description'] = $('#formDescription').val();
        formData['data']['content'] = quill.root.innerHTML;
        formData['data']['public'] = $('input[name="publicOptions"]:checked').val();
        if($('#formDatePublic').val()==''&&formData['data']['public']==1)
        {
          formData['data']['publicDate'] = dateNow()+' '+timeNow();
        }
        formData['data']['type'] = 'update-pages';
        //var jsonInputs = JSON.stringify(inputs);
        $.ajax({
            url: 'remoteUpdateScript/remoteUpdater.php',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $('#messageBox').html(data.message).trigger('update');
                window.location.href='pagesEditor.php?id='+formData['data']['id'];
            },
            data: formData
        });
      });
      //radio controller
      $('input[name="publicOptions"]').change(function() {
        if ($(this).val() == 'date') {
          $('#publicDate').show();
        } else {
          $('#publicDate').hide();
        }
      });
    });
    function dateNow(){
      var date = new Date();
      var day = date.getDate();
      var month = date.getMonth() + 1;
      var year = date.getFullYear();

      if (month < 10) month = "0" + month;
      if (day < 10) day = "0" + day;

      var today = year + "-" + month + "-" + day;
      return today;
    }
    function timeNow(){
      var date = new Date();
    var hour = date.getHours();
    var minute = date.getMinutes();
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    var time = hour + ':' + minute;
    return time;
    }
  </script>
</body>

</html>
