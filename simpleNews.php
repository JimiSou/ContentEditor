<?php include('safe.php');?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>CE-Aktuality</title>
  <link rel="stylesheet" type="text/css" href="css/loader.css">
  <script src="scripts/loader.js"></script>
  <link rel="stylesheet" type="text/css" href="css/Template_Lumen/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/contentEditorStyles.css">
  <link rel="stylesheet" type="text/css" href="css/icofont.css">
<link href="css/quill.snow.css" rel="stylesheet">

</head>

<body>
  <div id="wrapper">
    <nav></nav>

    <section>
        <h1>Aktuality</h1>
        <form method="post" id="form1">
          <h2>Česky</h2>
          <div class="form-group">
            <div id="editorCs">
            </div>
          </div>
          <br>
          <button type="sumbit" class="btn btn-success btn-lg" name="save">Uložit</button>
        </form>
        <br>
        <form method="post" id="form2">
          <h2>Anglicky</h2>
          <div class="form-group">
            <div id="editorEn">
            </div>
          </div>
          <br>
          <button class="btn btn-success btn-lg" type="sumbit" name="save2">Uložit</button>
        </form>
        <br>
    </section>
    <div class="alert alert-dismissible alert-secondary">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <span id="messageBox"></span>
</div>
  </div>
  <div id="loader"><img src="images/loader.svg"></div>
  <script src="scripts/global.min.js"></script>
  <script src="scripts/quill.min.js"></script>

  <script>
    $(function() {
      $('.breadcrumb').append('<li class="breadcrumb-item active">Aktuality</li>');

//onload end
    });
  //quill init variables
  var number=2;
  var editorSelector=['#editorCs','#editorEn'];
  var dataSource=['data/simpleNewsCs.json?'+ (new Date()).getTime(),'data/simpleNewsEn.json?'+ (new Date()).getTime()];
  var formSelector=['#form1','#form2'];
  var updaterEvents=['save-news-cs','save-news-en'];
  var quill = [];
  </script>
  <script src="scripts/quillInit.js"></script>
  <script>
  $('#form1').on('submit', function(e) {
      e.preventDefault();
      var formData = {'data':{'content':'','type':''}};
      formData['data']['content'] = quill[0].root.innerHTML;
      formData['data']['type'] = 'save-news-cs';
      $.ajax({
          url: 'remoteUpdateScript/remoteUpdater.php',
          type: 'post',
          dataType: 'json',
          success: function (data) {
              $('#messageBox').html(data.message).trigger('update');
          },
          data: formData
      });
    });
    $('#form2').on('submit', function(e) {
        e.preventDefault();
        var formData = {'data':{'content':'','type':''}};
        formData['data']['content'] = quill[1].root.innerHTML;
        formData['data']['type'] = 'save-news-en';
        $.ajax({
            url: 'remoteUpdateScript/remoteUpdater.php',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $('#messageBox').html(data.message).trigger('update');
            },
            data: formData
        });
      });
  </script>
</body>

</html>
