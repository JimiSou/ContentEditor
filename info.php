<?php include ('safe.php');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CE</title>
<link rel="stylesheet" type="text/css" href="css/loader.css">
<script src="scripts/loader.js"></script>
<link rel="stylesheet" type="text/css" href="css/Template_Lumen/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/contentEditorStyles.css">
<link rel="stylesheet" type="text/css" href="css/icofont.css">
</head>
<body>
<div id="pageInfo" >
  <nav></nav>
  <section style="text-align: center">
    <h1 class="text-primary">Verze</h1>
    <ul style="text-align: left">
      <li><h2>0.2</h2>
        <ul>
          <li>Přihlášení (jedno heslo)</li>
          <li>Editor stránek (vytváření, ukládání, editace)</li>
          <li>Editor článků (vytváření, ukládání, editace)</li>
          <li>Zprovoznění funkcí wysiwyg editoru (historie,upload obrázků,clipboard)</li>
        </ul>
      </li>
    </ul>
    <h1 class="text-primary">Použité Knihovny</h1>
    <div id="pageInfo-listOfPlugins">
      <ul>
        <li> <a href="https://getbootstrap.com/" target="_blank" class="btn btn-info"><img src="https://getbootstrap.com/assets/img/bootstrap-stack.png"> Bootstrap</a><label>HTML CSS framework</label> </li>
        <li> <a href="https://bootswatch.com/lumen/" target="_blank" class="btn btn-info "><img src="http://www.katieball.me/bootswatch/assets/img/logo.png"> Bootswatch Lumen</a><label>Template na Bootstrap</label> </li>
        <li> <a href="https://jquery.com/" target="_blank" class="btn btn-info "><img src="http://pluspng.com/img-png/jquery-logo-png--800.gif"> jQuery</a><label>Javascript Framework</label></li>
        <li> <a href="https://jqueryui.com/" target="_blank" class="btn btn-info "><img src="http://jqueryto.com/2013/assets/i/icon-jqueryui.png"> jQuery-ui</a> <label>jQuery framework ui</label></li>
        <li> <a href="https://popper.js.org/" target="_blank" class="btn btn-info "><img src="https://raw.githubusercontent.com/FezVrasta/popper.js/master/popperjs.png"> Popper</a><label>Pro potřeby tooltipů</label></li>
        <li> <a href="https://formbuilder.online/" target="_blank" class="btn btn-info "><img src="https://formbuilder.online/assets/img/fb-logo.svg"> Form-Builder</a><label>jQuery plugin pro sestavování formulářů</label></li>
        <li> <a href="http://photoswipe.com/" target="_blank" class="btn btn-info ">Photoswipe</a><label>Javascript responzivní galerie</label></li>
        <li> <a href="https://quilljs.com/" target="_blank" class="btn btn-info "><img src="https://camo.githubusercontent.com/b0c238d8006d3d3f4a6bb199f44195c08bf70664/68747470733a2f2f7175696c6c6a732e636f6d2f6173736574732f696d616765732f6c6f676f2e737667"> Quill</a><label>Javascript wysiwyg plugin pro stylování obsahu</label></li>
        <li> <a href="http://www.formvalidator.net/" target="_blank" class="btn btn-info ">jQuery Form Validator</a><label>jQuery plugin pro validaci formulářů</label></li>

      </ul>
      </div>
  </section>
</div>
<div id="loader"><img src="images/loader.svg"></div>
<script src="scripts/global.min.js"></script>
<script>
$(function(){
  $('.breadcrumb').append('<li class="breadcrumb-item active">O Programu</li>');
});
</script>
</body>

</html>
