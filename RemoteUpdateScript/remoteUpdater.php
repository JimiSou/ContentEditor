<?php
$response = new \stdClass();
//local 1x, studioosm 1x, creativelearning 1x, divadloterno 1x
$whiteList = ['127.0.0.1','31.15.10.37','81.2.195.60','46.28.105.133']; //DENY LOCAL ON PROD!!!!
$incomingData=$_POST['data'];
//var_dump($incomingData);
if(incomingDataCheck($incomingData))
{
  $clientIp = getClientIp();
  if(in_array($clientIp , $whiteList))
  {
    $response->clientIp = $clientIp;
    if($clientIp == '127.0.0.1')
    {
      if($incomingData['type']=='update-pages'||$incomingData['type']=='update-articles')
      {
        $response->message = savePage($incomingData);
      }
      if($incomingData['type']=='delete-pages'||$incomingData['type']=='delete-articles')
      {
          $response->message = deletePage($incomingData['id'],$incomingData['type']);
      }
      if($incomingData['type']=='save-news-en')
      {
        $response->message = saveNews($incomingData['content'], 'en');
      }
      if($incomingData['type']=='save-news-cs')
      {
        $response->message = saveNews($incomingData['content'], 'cs');
      }
      if($incomingData['type']=='upload-image')
      {
        $imageResponse = uploadImage($incomingData['file']);
        if($imageResponse)
        {
            $response->message = $imageResponse->message;
            $response->url=$imageResponse->url;
        }
        else {
          $response->message = 'Nastala chyba, obrázek nebyl uložen. Zkuste to prosím znovu.';
        }
      }
    }
    else {
      $response->message = 'Chyba ověření IP. ';
    }
  }
  else {
    $response->message = 'Chyba autorizace IP příchozího serveru! ';
  }
}
else {
  $response->message = 'Chyba: Chyba ověření konzistence dat. ';
}

echo json_encode($response);







// Function to get the client ip address
function getClientIp()
{
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP'])) $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED'])) $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED'])) $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR'])) $ipaddress = $_SERVER['REMOTE_ADDR'];
  else $ipaddress = '';
  return $ipaddress;
}
//consistency test
function incomingDataCheck($incomingData){
  $check = 1;
  if(array_key_exists('type',$incomingData))
  {
    $type=$incomingData['type'];
    if($type=='upload-image')
    {
      $check = $check * (int) array_key_exists('file',$incomingData);
      if($check==1)
      {
        return true;
      }
      else {
        return false;
      }
    }
    if($type=='update-pages')
    {
      $check = $check * (int) array_key_exists('id',$incomingData);
      $check = $check * (int) array_key_exists('created',$incomingData);
      $check = $check * (int) array_key_exists('title',$incomingData);
      $check = $check * (int) array_key_exists('description',$incomingData);
      $check = $check * (int) array_key_exists('content',$incomingData);
      $check = $check * (int) array_key_exists('public',$incomingData);
      $check = $check * (int) array_key_exists('publicDate',$incomingData);
      $check = $check * (int) array_key_exists('inMenu',$incomingData);
      if($check==1)
      {
        return true;
      }
      else {
        return false;
      }
    }
    if($type=='save-news-cs'||$type=='save-news-en')
    {
      $check = $check * (int) array_key_exists('content',$incomingData);
      if($check==1)
      {
        return true;
      }
      else {
        return false;
      }
    }
    if($type=='delete-pages')
    {
      $check = $check * (int) array_key_exists('id',$incomingData);
      if($check==1)
      {
        return true;
      }
      else {
        return false;
      }
    }
    if($type=='update-articles')
    {
      $check = $check * (int) array_key_exists('id',$incomingData);
      $check = $check * (int) array_key_exists('created',$incomingData);
      $check = $check * (int) array_key_exists('title',$incomingData);
      $check = $check * (int) array_key_exists('description',$incomingData);
      $check = $check * (int) array_key_exists('content',$incomingData);
      $check = $check * (int) array_key_exists('public',$incomingData);
      $check = $check * (int) array_key_exists('publicDate',$incomingData);
      if($check==1)
      {
        return true;
      }
      else {
        return false;
      }
    }
    if($type=='delete-articles')
    {
      $check = $check * (int) array_key_exists('id',$incomingData);
      if($check==1)
      {
        return true;
      }
      else {
        return false;
      }
    }
  }
  else {
    return false;
  }


}
function savePage($incomingData)
{
  $returner;
  if($incomingData['type']=='update-pages')
  {
    $targetPath='../data/pages.json';
    $returner='Vše proběhlo OK Stránka '.$incomingData['id'].' uložena. ';
  }
  if($incomingData['type']=='update-articles') {
    $targetPath='../data/articles.json';
    $returner='Vše proběhlo OK Článek '.$incomingData['id'].' uložen. ';
  }
  $string = file_get_contents($targetPath);
  $json = json_decode($string, true);
  $output=array();
  if($json != [])
  {
    for($i=0; $i<count($json);$i++)
    {
      if($incomingData['id']!=$json[$i]['id'])
      {
        $output[]=$json[$i];
      }
    }
    $output[]=$incomingData;
  }
  else {
    $output=array();
    $output[]=$incomingData;
  }
  $fp = fopen($targetPath, 'w');
  $test = fwrite($fp, json_encode($output));
  fclose($fp);
  if($test){
    return $returner;
  }
  else {
    return 'Chyba: ukládání se nezdařilo';
  }
}
function deletePage($id,$type)
{
  $returner;
  $targtargetPath;
  if($type=='delete-pages')
  {
    $targetPath='../data/pages.json';
    $returner='Vše proběhlo OK Stránka '.$id.' smazána. ';
  }
  if($type=='delete-articles') {
    $targetPath='../data/articles.json';
    $returner='Vše proběhlo OK Článek '.$id.' smazán. ';
  }
  $string = file_get_contents($targetPath);
  $json = json_decode($string, true);
  $output=array();
  if($json != [])
  {
    for($i=0; $i<count($json);$i++)
    {
      if($id!=$json[$i]['id'])
      {
        $output[]=$json[$i];
      }

    }
    $fp = fopen($targetPath, 'w');
    fwrite($fp, json_encode($output));
    fclose($fp);
    return $returner;
  }else {
    return 'Chyba: Soubor je prázdný';

  }

}
function saveNews($value,$lang)
{
  $targetPath='../data/simpleNews'.ucfirst(strtolower($lang)).'.json';
  $output = new \stdClass();
  $output->content = $value;
  $fp = fopen($targetPath, 'w');
  fwrite($fp,json_encode($output));
  fclose($fp);
  return "Novinky ".$lang." uloženy!";
}
function uploadImage($file){
  $filepath;
  $returner = new \stdClass();


// baseFromJavascript will be the javascript base64 string retrieved of some way (async or post submited)
$baseFromJavascript = $file; // $_POST['base64']; //your data in base64 'data:image/png....';
// We need to remove the "data:image/png;base64,"
$base_to_php = explode(',', $baseFromJavascript);

// the 2nd item in the base_to_php array contains the content of the image
$data = base64_decode($base_to_php[1]);
$fileType=$base_to_php[0];
if($fileType)
{
if($fileType=='data:image/jpeg;base64')
{
  $name=generateRandomString(8).".jpeg";
  $filepath = "../data/uploads/".$name;
}elseif($fileType=='data:image/png;base64')
{
  $name=generateRandomString(8).".png";
  $filepath = "../data/uploads/".$name;
}

$check=file_put_contents($filepath,$data);
    if($check !== false) {
      $returner->message='Soubor byl úspěšně uložen. <a href="'.$filepath.'">'.$name.'</a>';
      $returner->url=$filepath;
      return $returner;
    } else {
      $returner->message='Nastala chyba, soubor nebyl uložen. Zkuste to prosím znovu.';
      $returner->url=false;
      return $returner;
    }
}else {
  $returner->message="Nastala chyba, obrázek musí být ve formátu jpeg nebo png!!";
  $returner->url=false;
  return $returner;
}

}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
 ?>
