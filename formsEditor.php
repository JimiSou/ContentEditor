<?php include ('safe.php');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CE</title>
<link rel="stylesheet" type="text/css" href="css/loader.css">
<script src="scripts/loader.js"></script>
<link rel="stylesheet" type="text/css" href="css/Template_Lumen/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/contentEditorStyles.css">
<link rel="stylesheet" type="text/css" href="css/icofont.css">
</head>
<body>
<div id="wrapper">
  <nav></nav>
  <section>
  <div id="editor"></div>
</section>
</div>
<div id="loader"><img src="images/loader.svg"></div>
<script src="scripts/global.min.js"></script>
<script src="scripts/form-builder.min.js"></script>
<script>
$(function(){
  $('.breadcrumb').append('<li class="breadcrumb-item"><a href="forms.html">Formuláře</a></li><li class="breadcrumb-item active">Upravit Formulář</li>');

  $(document.getElementById('editor')).formBuilder();
});
</script>
</body>

</html>
