<html lang="cs">
<head>
<meta charset="UTF-8">
<title>CE-Login</title>
<link rel="stylesheet" type="text/css" href="css/loader.css">
<script src="scripts/loader.js"></script>
<link rel="stylesheet" type="text/css" href="css/Template_Lumen/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/contentEditorStyles.css">
<link rel="stylesheet" type="text/css" href="css/icofont.css">
</head>
<body>


<?php
session_start();
include('login_check.php');
$loged_in = checkLogin();

//--------------------------------------------------------------------
//---------------Main Content-----------------------------------------
//--------------------------------------------------------------------
//--P¯ijetÌ a vypsanÌ zpravy

if ($loged_in){
	Echo ('You are logged in!');
	//--kontrola loginu a v p¯Ì˘adÏ nezalogov·nÌ vyps·nÌ formul·¯e
	Header('Location: new_cs.php');

}
else{
	?>

<div id="wrapper">

	<section id="loginPage">
			<h1>Příhlášení</h1>


				<form method="post" name="login_form" action="login_proces.php" class="login_form">

				<div class="form-group">
					<label class="col-form-label">Uživatelské jméno</label> <input class="form-control" type="text" name="login" class="login_field"/><br /><br/>
					<label class="col-form-label">Heslo</label> <input class="form-control" type="password" name="password" class="login_field"/><br /><br />
					<button class="card-link btn btn-primary" type="submit" class="button">Přihlásit</button>

        </div>
					</form>


	</section>
	<div class="alert alert-dismissible alert-secondary">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<span id="messageBox"></span>
</div>
</div>
<div id="loader"><img src="images/loader.svg"></div>
<script src="scripts/global.min.js"></script>
	<?php
}



//--------------------------------------------------------------------
//---------------Main Content end-------------------------------------
//--------------------------------------------------------------------
?>
</body>
</html>
