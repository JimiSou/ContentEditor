$(function() {
  const Clipboard = Quill.import('modules/clipboard');
  const History = Quill.import('modules/history');
  const Delta = Quill.import('delta');
  var toolbarOptions = [
    [{
      'font': []
    }],
    ['bold', 'italic', 'underline', 'strike'], // toggled buttons
    [{
      'color': []
    }, {
      'background': []
    }], // dropdown with defaults from theme
    // ['code-block'],
    [{
      'header': 1
    }, {
      'header': 2
    }], // custom button values
    [{
      'size': ['small', false, 'large', 'huge']
    }], // custom dropdown
    [{
      'align': []
    }],
    [{
      'list': 'ordered'
    }, {
      'list': 'bullet'
    }],
    // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    ['link', 'image'],
    ['clean'] // remove formatting button
  ];

  for (var i = 0; i < number; i++) {
    // Handlers can also be added post initialization
    quill[i] = new Quill(editorSelector[i], {
      modules: {
        toolbar: toolbarOptions,
        history: {
          delay: 2000,
          maxStack: 500,
          userOnly: true
        }
      },
      theme: 'snow'
    });
    quill[i].clipboard.addMatcher(Node.ELEMENT_NODE, function(node, delta) {
      var plaintext = $(node).text();
      return new Delta().insert(plaintext);
    });
    //redo buttons init
    $(formSelector[i] + ' div.ql-toolbar').prepend('<span><button id="redo' + i + '" type="button"><svg viewbox="0 0 18 18"><polygon class="ql-fill ql-stroke" points="12 10 14 12 16 10 12 10"></polygon><path class="ql-stroke" d="M9.91,13.91A4.6,4.6,0,0,1,9,14a5,5,0,1,1,5-5"></path></svg></button></span>');
    //undo buttons init
    $(formSelector[i] + ' div.ql-toolbar').prepend('<span><button id="undo' + i + '" type="button"><svg viewbox="0 0 18 18"><polygon class="ql-fill ql-stroke" points="6 10 4 12 2 10 6 10"></polygon><path class="ql-stroke" d="M8.09,13.91A4.6,4.6,0,0,0,9,14,5,5,0,1,0,4,9"></path></svg></button></span>');

    $('#undo' + i).on('click', {'i': i}, function(e) {
      var i = e.data.i;
      quill[i].history.undo();
    });
    $('#redo' + i).on('click', {'i': i}, function(e) {
      var i = e.data.i;
      quill[i].history.redo();
    });
    //text-change event

    quill[i].on('text-change', function(delta) {
      //FOR delta find insert:image
      var array = delta['ops'];
      var src;
      for (var i = 0; i < array.length; i++) {
        if('insert' in array[i])
        {
          if(typeof array[i]['insert'] === 'object')
          {
            if('image' in array[i]['insert'])
            {
            if (array[i]['insert']['image'].indexOf('data:image/')>=0) {

              src=array[i]['insert']['image'];
              $('#messageBox').html('Obrázek se nahrává...').trigger('update');
              var img = $('img[src="'+src+'"]');
              img.prop('src', 'images/loader.svg');
              var formData = {
                'data': {
                  'file': '',
                  'type': ''
                }
              };
              formData['data']['file'] = src;
              formData['data']['type'] = 'upload-image';
              $.ajax({
                url: 'remoteUpdateScript/remoteUpdater.php',
                type: 'post',
                dataType: 'json',
                success: function(data) {
                  $('#messageBox').html(data.message).trigger('update');
                  if (data.url != false) {
                    img.prop('src', data.url);
                  }
                },
                data: formData,
              });
              }
            }
          }
        }
      }


    });
    //Loading from file
    $.ajax({
        async: false,
        url: dataSource[i],
        dataType: "json",
        success: function(data){
          //+console.log(data);
            quill[i].root.innerHTML = data.content;
        }
    });
  }
});
