<?php include ('safe.php');?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>CE</title>
  <link rel="stylesheet" type="text/css" href="css/loader.css">
  <script src="scripts/loader.js"></script>
  <link rel="stylesheet" type="text/css" href="css/Template_Lumen/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/contentEditorStyles.css">
  <link rel="stylesheet" type="text/css" href="css/icofont.css">
</head>

<body>
  <div id="wrapper">
    <nav></nav>

    <section id="pagePages">
        <h1>Stránky</h1>
      <a href="pagesEditor.php" class="btn btn-outline-success btn-block mb-5">
        <big class="flex-vertical-middle"><span>Nová Stránka</span> <i class="icofont icofont-ui-add"></i></big>
      </a>

      <div class="card border-primary d-none p-0 col-sm-12 col-md-6 col-lg-3 mb-3" id="pageCardPrototype">
        <div class="card-header"><span id="pageCardId" class="text-muted">ID</span> <span id="pageCardMenuOrder" hidden>MENUORDER</span> <span class="text-muted" id="pageCardDate">DATE</span><button data-id class="card-link btn btn-danger" id="pageDeleteButton"><i class="icofont icofont-ui-delete"></i></button></div>
        <div class="card-body">
          <h4 class="card-title" id="pageCardTitle">TITLE</h4>
          <p class="card-text" id="pageCardDescription">DESCRIPTION</p>
        </div>
        <div class="card-body text-center">
          <a href="#" class="card-link btn btn-primary" id="pageCardEditLink">Upravit stránku</a>
        </div>
      </div>

    </section>
    <div class="alert alert-dismissible alert-secondary">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <span id="messageBox"></span>
</div>
  </div>
  <div id="loader"><img src="images/loader.svg"></div>
  <script src="scripts/global.min.js"></script>
  <script>
    $(function() {
      $('.breadcrumb').append('<li class="breadcrumb-item active">Stránky</li>');
      //LOADING PAGES
      $.getJSON("data/pages.json?"+ (new Date()).getTime(), function(result) {
        if (result) {
          var output=$('<div>');
          var template = $('#pageCardPrototype');
          $.each(result, function(i, field) {
            //console.log(field);
              var ele = $('<div>').append($(template).clone());
              ele.find('#pageCardTitle').text(field.title);
              if(field.description.length > 200)
              {
                ele.find('#pageCardDescription').text(field.description.substring(0, 200)+' ...');
              }
              else {
                ele.find('#pageCardDescription').text(field.description);

              }
              ele.find('#pageCardDate').text(field.created);
              ele.find('#pageCardMenuOrder').text(field.order);
              ele.find('#pageCardId').text(field.id);
              ele.find('#pageCardEditLink').attr('href','pagesEditor.php?id='+field.id);
              ele.find('#pageDeleteButton').attr('data-id',field.id);
              ele.find('.card').removeClass('d-none');
              //ele.find('.card').attr('id',field.id);
              output.append(ele.html());
          }
        );

        $('#pagePages').append(output.html());
        }
        else {
          console.log('fail');
          $('#pagePages').append('<h2 class="text-info">Nejsou vytvořeny žádné stránky</h2>');
        }

      });

      $(document).on('click','#pageDeleteButton',function(e) {
        e.preventDefault();
        if (confirm("Opravdu chcete stránku odstranit?!")) {
          var formData = {'data':{'id':'','type':''}};
            formData['data']['type'] = 'delete-pages';
            formData['data']['id'] = $(this).attr('data-id');
            $.ajax({
                url: 'remoteUpdateScript/remoteUpdater.php',
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    $('#messageBox').html(data.message).trigger('update');
                    window.location.reload();
                },
                data: formData
            });
        }
      });
    });
  </script>
</body>

</html>
